# Ad Tech Challenge Frontend

Lightweight Frontend testing framework for the FE challenge

## Tech stack

- [ ] [Java](https://dev.java/)
- [ ] [Maven](https://maven.apache.org/)
- [ ] [Cucumber](https://cucumber.io/)
- [ ] [Selenium](https://www.selenium.dev/)
- [ ] [TestNG](https://testng.org/doc/)

### Project Structure
```
.
├── README.md
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   └── resources
│   └── test
│       ├── java
│       │   ├── StepDefinitions
│       │   │   ├── OnlineShopSteps.java
│       │   │   ├── VerificationSteps.java
│       │   │   └── base
│       │   │       ├── BaseSteps.java
│       │   │       └── context
│       │   │           └── Context.java
│       │   ├── driver
│       │   │   ├── DriverFactory.java
│       │   │   ├── drivers
│       │   │   │   ├── linux
│       │   │   │   ├── mac
│       │   │   │   └── windows
│       │   │   └── utils
│       │   │       ├── DataLoader.java
│       │   │       ├── Globals.java
│       │   │       └── PropertyUtils.java
│       │   ├── pageObjects
│       │   │   ├── CartPO.java
│       │   │   ├── HomePO.java
│       │   │   ├── NavBarPO.java
│       │   │   ├── OrderPO.java
│       │   │   ├── ProductPO.java
│       │   │   └── base
│       │   │       └── BasePO.java
│       │   └── runner
│       │       └── Run.java
│       └── resources
│           ├── develop.properties
│           ├── production.properties
│           ├── specs
│           │   └── demo_online_shop.feature
│           └── staging.properties
└── testng.xml
```

# Running the project

## Preconditions
- in order to run Java +1.8 and Maven 3 are required locally
- clone the project by running: <br>
  `git clone https://gitlab.com/murreli1981/ad-tech-challenge-frontend.git`
- copy any firefox, chrome or safari driver into the folder: <br>
  `src/test/java/driver/drivers/<<OS>>/`
- into the project folder, run: <br>
  `mvn clean test`

### properties
there are two 3 property files within the framework<br>
`develop.properties`, `production.properties` and `staging.properties` which contain:

- `browser=` ## corresponding to the browser type (chrome / firefox / safari)
- `remote=` ## any selenium grid URL
- `site_url=` ## web site URL under test

*Note*

`remote`: if the remote property is filled with any value the framework will try to run it in remote mode first
also if the `remote` property is empty then the framework will try to run it locally using the local drivers previously placed in folders `src/test/java/driver/drivers/<<OS>>/`
`browser`: will select and create DriverOptions for the 3 browsers supported, the value selected will apply for both remote and local modes
  * firefox
  * chrome
  * safari

### run opts
the projects run using mvn command, there are three possible flavours: <br>
`mvn clean test -Denvironment=prod` uses the configuration created for production env<br>
`mvn clean test -Denvironment=staging` uses the configuration created for staging env<br>
`mvn clean test <<-Denvironment=develop>>` uses the develop configuration (default)<br>

### specs
the package `src/test/resources/specs/` contains the .feature files, if new features / scenarios are expected they should be added here:

### Step Definitions
the glue code is placed into the package `src/test/java/StepDefinitions` along with a CustomAssert class

### Reports

The framework provides the standard cucumber report placed locally in `target/Cucumber.html`, <br>

[Cucumber Report](Cucumber.html)

<img height = 500px src="https://gitlab.com/murreli1981/ad-tech-challenge-frontend/-/raw/main/src/test/resources/screenshot/cucumber.png">
## Framework

the`src/test/java/driver` package contains:
- `DriverFactory` responsible for create the webDriver instance based on the configurations
- `/drivers` folder where the local drivers should be placed

## Utils

the `src/test/java/utils` package contains util classes for property data loading

## StepDefinitions

this package contain the glue code and a context class (DI through Pico Container) for storing the transaction ID
(it will be send to the Cucumber Report after finishing the execution)


## Page Objects

attributes and methods represented for the tests to run

## Resources

Package that contains .feature file and framework properties


___
Marcelo Urreli - 2022
