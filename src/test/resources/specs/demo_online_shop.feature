@Demo_Online_Shop
Feature: Demo Online Shop

  Scenario Outline:
    Given I go to the demo online shop
    When I navigate to the category Laptops
    When I navigate to product "<firstProduct>"
    And click to "Add to cart"
    And I accept the confirmation message
    And I go back to home
    When I navigate to the category Laptops
    And I navigate to product "<secondProduct>"
    And click to "Add to cart"
    And I accept the confirmation message
    And I go back to home
    And I go to the Cart
    And I remove the "<secondProduct>"
    And I place the order
    And I complete the purchase data
    And click on Purchase
    Then the purchase is completed
    And the amount is equals 790




    Examples:
    | firstProduct | secondProduct |
    | Sony vaio i5 | Dell i7 8gb   |

    ##vaio 790 / dell 700