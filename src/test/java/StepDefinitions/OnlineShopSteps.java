package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import pageObjects.*;

public class OnlineShopSteps {

    HomePO homePO;
    ProductPO productPO;
    NavBarPO navBarPO;
    CartPO cartPO;
    OrderPO orderPO;

    public OnlineShopSteps(HomePO homePO, ProductPO productPO, NavBarPO navBarPO, CartPO cartPO, OrderPO orderPO) {
        this.homePO = homePO;
        this.productPO = productPO;
        this.navBarPO = navBarPO;
        this.cartPO = cartPO;
        this.orderPO = orderPO;
    }

    @Given("I go to the demo online shop")
    public void i_go_to_the_demo_online_shop() {
        homePO.navigateToHome();
    }

    @When("I navigate to the category {word}")
    public void i_navigate_to_the_category(String category) {
        homePO.findCategoryByName(category);
    }

    @When("I navigate to product {string}")
    public void i_navigate_to(String product1) {
        homePO.findProductByName(product1);
    }

    @When("click to {string}")
    public void click_to(String option) {
        productPO.addProduct(option);
    }

    @When("I accept the confirmation message")
    public void i_accept_the_confirmation_message() {
        productPO.acceptPopUp();
    }

    @When("I go back to home")
    public void i_go_back_to_home() { navBarPO.navigateToHome(); }

    @When("I go to the Cart")
    public void i_go_to_the_cart() {
        navBarPO.navigateToCart();
    }

    @When("I remove the {string}")
    public void i_remove_the(String product) { cartPO.removeProduct(product); }

    @When("I place the order")
    public void i_place_the_order() {
        cartPO.placeOrder();
    }

    @When("I complete the purchase data")
    public void i_complete_the_purchase_data() { orderPO.fillOrder(); }

    @When("click on Purchase")
    public void click_on_purchase() {
        orderPO.clickOnPurchase(); }

}
