package StepDefinitions;

import StepDefinitions.base.context.Context;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import pageObjects.OrderPO;

public class VerificationSteps {

    OrderPO orderPO;
    Context context;

    public VerificationSteps(OrderPO orderPO, Context context) {
        this.orderPO = orderPO;
        this.context = context;
    }

    @Then("the purchase is completed")
    public void the_purchase_is_completed() {
        String transactionId = orderPO.getTransactionId();
        Assert.assertTrue(transactionId.length() > 0);
        context.transaction = transactionId;

    }
    @Then("the amount is equals {int}")
    public void the_amount_is_equals(int expectedAmount) {
        int amount = orderPO.getAmount();
        Assert.assertEquals(amount, expectedAmount);
    }
}
