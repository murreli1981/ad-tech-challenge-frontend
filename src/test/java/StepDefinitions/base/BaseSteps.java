package StepDefinitions.base;

import StepDefinitions.base.context.Context;
import com.google.common.net.MediaType;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.sql.Timestamp;

import static driver.DriverFactory.cleanupDriver;
import static driver.DriverFactory.getDriver;

public class BaseSteps {

    Context context;
    public BaseSteps(Context context) {
        this.context = context;
    }
    @Before
    public void setup() {
        getDriver();
    }

    @AfterStep
    public void captureExceptionImage(Scenario scenario) {
        if (scenario.isFailed()) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String timeMilliseconds = Long.toString(timestamp.getTime());

            byte[] screnshot = ((TakesScreenshot) getDriver())
                    .getScreenshotAs(OutputType.BYTES);
            scenario.attach(screnshot, "image/png", timeMilliseconds);
        }
    }

    @After
    public void tearDown(Scenario scenario) {
        if (!scenario.isFailed()) {
          scenario.attach("Transaction ID: " +
                  context.transaction, String.valueOf(MediaType.ANY_TEXT_TYPE),"value");
        }
        cleanupDriver();
    }
}
