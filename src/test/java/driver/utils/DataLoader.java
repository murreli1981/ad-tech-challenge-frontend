package driver.utils;

import com.sun.javafx.fxml.PropertyNotFoundException;

import java.io.File;
import java.util.Properties;

public class DataLoader {

    private final Properties properties;
    private static DataLoader configLoader;
    private final String PROPERTIES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;

    private DataLoader() {

        String env = System.getProperty("environment");

        if (env == null) {
            properties = PropertyUtils.propertyLoader(PROPERTIES_PATH + "develop.properties");
        } else
            switch (env) {
                case "production":
                    properties = PropertyUtils.propertyLoader(PROPERTIES_PATH + "production.properties");
                    break;
                case "staging":
                    properties = PropertyUtils.propertyLoader(PROPERTIES_PATH + "staging.properties");
                    break;
                default:
                    properties = PropertyUtils.propertyLoader(PROPERTIES_PATH + "develop.properties");
                    break;
            }
    }

    public static DataLoader getInstance() {
        if (configLoader == null) {
            configLoader = new DataLoader();
        }
        return configLoader;
    }

    public String getSiteUrl() {
        String prop = properties.getProperty("site_url");
        if (prop != null) return prop;
        else throw new PropertyNotFoundException("property [site_url] not found");
    }

    public String getBrowser() {
        String prop = properties.getProperty("browser");
        if (prop != null) return prop;
        else throw new PropertyNotFoundException("property [browser] not found");
    }

    public String getRemote() {
        String prop = properties.getProperty("remote");
        if (prop != null) return prop;
        else throw new PropertyNotFoundException("property [remote] not found");
    }
}

