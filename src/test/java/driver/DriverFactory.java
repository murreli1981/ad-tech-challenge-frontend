package driver;

import driver.utils.DataLoader;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.AbstractDriverOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;

import static java.io.File.separator;
import static org.apache.commons.lang3.SystemUtils.*;

public class DriverFactory {

    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

    public static WebDriver getDriver() {
        if (webDriver.get() == null) {
            webDriver.set(createDriver());
        }

        return webDriver.get();
    }

    public static WebDriver createDriver() {

        WebDriver driver = null;

        String driverUrl = separator + "src" +
                separator + "test" +
                separator + "java" +
                separator + "driver" +
                separator + "drivers" +
                separator;

        if (IS_OS_MAC) driverUrl += "mac" + separator;
        else if (IS_OS_LINUX) driverUrl += "linux" + separator;
        else if (IS_OS_WINDOWS) driverUrl += "windows" + separator;
        if (DataLoader.getInstance().getRemote().isEmpty()) {

            switch (DataLoader.getInstance().getBrowser()) {
                case "chrome": {
                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + driverUrl +
                            "chromedriver" + (IS_OS_WINDOWS ? ".exe" : ""));
                    driver = new ChromeDriver(getChromeOptions());
                    break;
                }
                case "safari": {
                    System.setProperty("webdriver.safari.driver", System.getProperty("user.dir") + driverUrl +
                            "safaridriver");
                    driver = new SafariDriver(getSafariOptions());
                    break;
                }
                case "firefox": {
                    System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + driverUrl +
                            "geckodriver" + (IS_OS_WINDOWS ? ".exe" : ""));
                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    firefoxOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                    driver = new FirefoxDriver(getFirefoxOptions());
                    break;
                }

                default: {
                    throw new UnsupportedOperationException("Browser type not supported: " +
                            DataLoader.getInstance().getBrowser());
                }
            }
        } else {
            try {
                driver = new RemoteWebDriver(new URL(DataLoader.getInstance().getRemote()), getDriverOpts());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        driver.manage().window().maximize();
        return driver;
    }

    public static AbstractDriverOptions getDriverOpts() {
        AbstractDriverOptions opts = null;
        switch (DataLoader.getInstance().getBrowser()) {
            case "chrome": {
                opts = getChromeOptions();
                break;
            }
            case "safari": {
                opts = getSafariOptions();
                break;
            }

            case "firefox": {
                opts = getFirefoxOptions();
                break;
            }
            default: {
                throw new UnsupportedOperationException("Browser type not supported: " +
                        DataLoader.getInstance().getBrowser());
            }

        }
        return opts;
    }

    private static SafariOptions getSafariOptions() {
        return new SafariOptions().setPageLoadStrategy(PageLoadStrategy.NORMAL);
    }
    private static FirefoxOptions getFirefoxOptions() {
        return new FirefoxOptions().setPageLoadStrategy(PageLoadStrategy.NORMAL);
    }

    private static ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        return chromeOptions;
    }

    public static void cleanupDriver() {
        webDriver.get().quit();
        webDriver.remove();
    }

}
