package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = {"classpath:specs"},
        glue = {"StepDefinitions"},
        monochrome = true,
        dryRun = false,
        tags = "@Demo_Online_Shop",
        plugin = {"pretty", "html:target/cucumber.html"})
public class Run extends AbstractTestNGCucumberTests {
    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() { return super.scenarios(); }
}
