package pageObjects.base;

import driver.DriverFactory;
import driver.utils.Globals;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

public class BasePO {

    JavascriptExecutor js;

    public BasePO() {
        PageFactory.initElements(getDriver(), this);
        this.js = (JavascriptExecutor) getDriver();
    }

    protected void sendKeys(WebElement element, String content) {
        String script = "document.getElementById('"+element.getAttribute("id")+"').value='"+content+"'";
        js.executeScript(script);
    }

    public WebDriver getDriver() {
        return DriverFactory.getDriver();
    }

    public void waitForWebElements(List<WebElement> elements) {
        try {
            getWait().until(ExpectedConditions.visibilityOfAllElements(elements));
        } catch (StaleElementReferenceException e) {
            waitForWebElements(elements);
        }
    }

    public void acceptPopUp() {
        getWait().until(ExpectedConditions.alertIsPresent()).getText();
        getDriver().switchTo().alert().accept();
    }

    public void waitForWebElementAndClick(By by) {
        try {
            getWait().until(ExpectedConditions.elementToBeClickable(by)).click();
        } catch (StaleElementReferenceException e) {
            System.out.println("element staled, retrying...");
            waitForWebElementAndClick(by);
        }
    }

    protected boolean waitForWebElementNoLongerVisible(WebElement element) {
        boolean removed = false;
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(30L))
                .pollingEvery(Duration.ofSeconds(5L))
                .ignoring(NoSuchElementException.class);
        while(!removed)
            removed = wait.until(ExpectedConditions.invisibilityOf(element));
        return removed;
    }

    public void waitForWebElementAndClick(WebElement webElement) {
        try {
            getWait().until(ExpectedConditions.visibilityOf(webElement)).click();
        } catch (StaleElementReferenceException e) {
            System.out.println("element staled, retrying...");
            waitForWebElementAndClick(webElement);
        }
    }

    protected WebElement findElementByText(List<WebElement> webElements, String text) {
        String textSanitized = text.replace("\"", "");
            return webElements
                    .stream()
                    .filter(w -> w.getText().equals(textSanitized))
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException("No WebElement found containing " + text));
    }

    protected WebElement waitForAsyncLoad(By by) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(30L))
                .pollingEvery(Duration.ofSeconds(5L))
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(by);
            }
        });
        return element;
    }

    protected WebElement getParent(WebElement child) {
        return child.findElement(By.xpath("./.."));
    }

    private WebDriverWait getWait() {
        return new WebDriverWait(getDriver(), Duration.ofSeconds(Globals.DEFAULT_EXPLICIT_TIMEOUT));
    }

}
