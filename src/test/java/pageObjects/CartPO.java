package pageObjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.base.BasePO;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

public class CartPO extends BasePO {

    @FindBy(xpath = "//button[text()='Place Order']")
    public WebElement button_placeOrder;

    public void placeOrder() {
        waitForWebElementAndClick(button_placeOrder);
    }

    public void removeProduct(String product) {
        WebElement element = getProductInTable(product);
        WebElement parent = getParent(element);
        WebElement deleteBtn = parent.findElements(By.tagName("td")).
                get(getTableColumnIndexFor("x"))
                .findElement(By.cssSelector("a"));
        waitForWebElementAndClick(deleteBtn);
        waitForWebElementNoLongerVisible(parent);
    }

    private WebElement getProductInTable(String product) {
        return waitForAsyncLoad(By.xpath("//*[@id='tbodyid']/tr/td[text()='" + product + "']"));
    }

    private int getTableColumnIndexFor(String title) {
        WebElement element = waitForAsyncLoad(By.cssSelector("table > thead > tr"));
            List<WebElement> elems = element.findElements(By.cssSelector("th"));
        return IntStream.range(0, elems.size())
                .filter(i -> title.equals(elems.get(i).getText()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No Such Column Present: " + title));
    }
}
