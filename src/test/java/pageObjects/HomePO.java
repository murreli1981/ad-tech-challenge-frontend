package pageObjects;

import driver.utils.Globals;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import pageObjects.base.BasePO;

import java.util.List;

public class HomePO  extends BasePO {

    @FindBys(@FindBy(xpath = "//a[@id='itemc']"))
    List<WebElement> categoryItems;

    public void navigateToHome() {
        getDriver().get(Globals.DEMOBLAZE_URL);
    }

    public void findCategoryByName(String name) {
        waitForWebElements(categoryItems);
        WebElement element = findElementByText(categoryItems, name);
        waitForWebElementAndClick(element);
    }

    public void findProductByName(String name) {
        try {
            waitForAsyncLoad(By.xpath("//a[text()=\"" + name + "\"]")).click();
        } catch (StaleElementReferenceException e) {
            waitForAsyncLoad(By.xpath("//a[text()=\"" + name + "\"]")).click();
        }
    }
}

