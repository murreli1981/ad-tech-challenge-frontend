package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import pageObjects.base.BasePO;

import java.util.List;

public class NavBarPO extends BasePO {
    @FindBy(id = "cartur")
    private WebElement cart;

    @FindBy(id ="nava")
    private WebElement brandLogo;

    public void navigateToCart() {
        waitForWebElementAndClick(cart);
    }

    public void navigateToHome() {
        waitForWebElementAndClick(brandLogo);
    }
}

