package pageObjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.base.BasePO;
import java.util.Date;

public class OrderPO extends BasePO {

    @FindBy(id = "name")
    public WebElement formInput_name;

    @FindBy(id = "country")
    public WebElement formInput_country;

    @FindBy(id = "city")
    public WebElement formInput_city;

    @FindBy(id = "card")
    public WebElement formInput_card;

    @FindBy(id = "month")
    public WebElement formInput_month;

    @FindBy(id = "year")
    public WebElement formInput_year;

    @FindBy(xpath = "//button[text()='Purchase']")
    public WebElement formButton_purchase;

    @FindBy(className = "lead")
    public WebElement successText_purchase;

    public void fillOrder() {
        sendKeys(formInput_country, "Test Country " + RandomStringUtils.randomNumeric(2));
        sendKeys(formInput_city, "Test City " + RandomStringUtils.randomNumeric(2));
        sendKeys(formInput_name, "Test Name " + RandomStringUtils.randomNumeric(2));
        sendKeys(formInput_card, "" + RandomStringUtils.randomNumeric(16, 17));
        sendKeys(formInput_month, "" + (new Date().getMonth() + 1));
        sendKeys(formInput_year, "2022");
    }

    public void clickOnPurchase() {
        waitForWebElementAndClick(formButton_purchase);
    }

    public String getTransactionId() {
        return successText_purchase.getText().split(" ")[1].replace("Amount:", "");
    }

    public int getAmount() {
        String[] elems = successText_purchase.getText().split(" ");
        return Integer.valueOf(elems[2]);
    }
}
