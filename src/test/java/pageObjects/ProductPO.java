package pageObjects;

import org.openqa.selenium.By;
import pageObjects.base.BasePO;

public class ProductPO extends BasePO {

    public void addProduct(String text) {
        waitForWebElementAndClick(By.xpath("//*[text()='"+text+"']"));
    }

}
